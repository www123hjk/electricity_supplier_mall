/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.5.53 : Database - xbshop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xbshop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `xbshop`;

/*Table structure for table `xb_category` */

DROP TABLE IF EXISTS `xb_category`;

CREATE TABLE `xb_category` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cname` varchar(300) NOT NULL COMMENT '分类名称',
  `parent_id` smallint(6) DEFAULT NULL COMMENT '分类的父分类id',
  `isrec` tinyint(4) DEFAULT '0' COMMENT '是否推荐',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `xb_category` */

insert  into `xb_category`(`id`,`cname`,`parent_id`,`isrec`) values 
(1,'电器',0,0),
(7,'手机',0,0),
(8,'生活用品',0,0),
(9,'洗衣机',1,0),
(10,'苹果',7,0),
(11,'毛巾',8,0),
(12,'彩电',1,0);

/*Table structure for table `xb_goods` */

DROP TABLE IF EXISTS `xb_goods`;

CREATE TABLE `xb_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(50) NOT NULL DEFAULT '',
  `goods_sn` char(30) NOT NULL DEFAULT '' COMMENT '商品货号',
  `cate_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '商品分类ID  对于category表中的ID字段',
  `market_price` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '市场售价',
  `shop_price` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '本店售价',
  `goods_img` varchar(50) NOT NULL DEFAULT '' COMMENT '商品缩略图',
  `goods_thumb` varchar(50) NOT NULL DEFAULT '' COMMENT '商品缩略小图',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否热卖 1表示热卖 0表示不热卖',
  `is_rec` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否推荐 1表示推荐 0表示不推荐',
  `is_new` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是新品 1表示是新品 0表示不是新品',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `is_del` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否删除 1表示正常 0表示删除',
  `is_sale` tinyint(4) NOT NULL DEFAULT '1' COMMENT '商品是否销售 1销售 0下架',
  `goods_body` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods_sn` (`goods_sn`) COMMENT '货号不能重复',
  KEY `goods_name` (`goods_name`),
  KEY `is_del` (`is_del`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `xb_goods` */

insert  into `xb_goods`(`id`,`goods_name`,`goods_sn`,`cate_id`,`market_price`,`shop_price`,`goods_img`,`goods_thumb`,`is_hot`,`is_rec`,`is_new`,`addtime`,`is_del`,`is_sale`,`goods_body`) values 
(1,'鞋子','',8,80,50,'','',1,0,0,0,1,1,''),
(2,'袜子','XB5bf7a428c6220',8,20,10,'','',0,0,1,1542956072,1,0,'666'),
(3,'vivo','1',7,3000,2000,'','',0,1,0,1542958697,1,1,'&lt;p&gt;物美价廉&lt;img src=&quot;/ueditor/php/upload/image/20181123/1542958695188436.png&quot; title=&quot;1542958695188436.png&quot; alt=&quot;c432b3413a8f783886936ca922479b984f09c2a22808-ameKq6_fw658.png&quot;/&gt;&lt;/p&gt;'),
(4,'sss','5',1,300,50,'','',0,0,0,1542962482,1,1,''),
(5,'收到','XB5bf7bd7739a40',1,30,63,'','',0,0,1,1542962551,1,1,''),
(6,'rr','XB5bf7c0dc09b6d',1,320,30,'Uploads/2018-11-23/5bf7c0dc0a725.png','Uploads/2018-11-23/thumb_5bf7c0dc0a725.png',0,0,0,1542963420,1,1,''),
(7,' 圣诞快乐','XB5bf8143a7c95e',1,333,22,'Uploads/2018-11-23/5bf8143a7da68.jpeg','Uploads/2018-11-23/thumb_5bf8143a7da68.jpeg',0,0,0,1542984762,1,1,'');

/*Table structure for table `xb_goods_cate` */

DROP TABLE IF EXISTS `xb_goods_cate`;

CREATE TABLE `xb_goods_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品ID标识',
  `cate_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '分类ID标识',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `xb_goods_cate` */

insert  into `xb_goods_cate`(`id`,`goods_id`,`cate_id`) values 
(1,7,9);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
