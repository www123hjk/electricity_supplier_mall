<?php
namespace Admin\Controller;

class GoodsController extends CommonController {
  	
	//执行数据添加入库的操作
  	public function add(){
  		if(IS_GET){
  			//获取所有的分类信息
  			$cate = D('Category') -> getCateTree();
			$this -> assign('cate',$cate);
  			$this -> display();
			exit();
  		}
		$model = D('Goods');
		$data = $model -> create();
		if(!$data){
			$this -> error($model -> getError());
		}
		$goods_id = $model -> add($data);
		if(!$goods_id){
			$this -> error('添加失败');
		}
		$this -> success('添加成功');
  	}
  	//商品列表显示操作
  	public function index(){
  		$model = D(Goods);
  		//调用模型方法获取数据
  		$data = $model -> listData();
  		$this -> assign('data',$data);
  		$this -> display();
  	}
}